# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Wallet.create!([
  {name: "User"},
  {name: "Team"},
  {name: "Stock"}
])

Group.create!([
  { name: "Steve Jobs", balance: 1000, wallet_id: 1, type: 'User' },
  { name: "Bill Gates", balance: 1200, wallet_id: 1, type: 'User' },
  { name: "Elon Musk", balance: 980, wallet_id: 1, type: 'User' },
  { name: "Mark Sopacua", balance: 750, wallet_id: 1, type: 'User' },
  { name: "Apple Inc", balance: 7050, wallet_id: 2, type: 'Team' },
  { name: "Microsoft", balance: 1250, wallet_id: 2, type: 'Team' },
  { name: "APPL", balance: 750, wallet_id: 3, type: 'Stock' },
  { name: "ASTR", balance: 250, wallet_id: 3, type: 'Stock' },
  { name: "BBLC", balance: 550, wallet_id: 3, type: 'Stock' }
])
