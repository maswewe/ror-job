class CreateGroups < ActiveRecord::Migration[6.0]
  def change
    create_table :groups do |t|
      t.string :name
      t.integer :balance
      t.integer :wallet_id
      t.string :type

      t.timestamps
    end
  end
end
