class BalanceController < ApplicationController
  def index
    @wallets = Group.all
    @users = Group.where(type: "User")
    @teams = Group.where(type: "Team")
    @stocks = Group.where(type: "Stock")
  end
end
