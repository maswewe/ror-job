class TransferController < ApplicationController

  def index
    @users = Group.where(type: "User")
    @teams = Group.where(type: "Team")
  end

  def sender_type
    if params[:type].present?
      @users = Group.where(type: params[:type])
    end
    if request.xhr?
      respond_to do |format|
        format.json {
          render json: @users
        }
      end
    end
  end

  def sender_name
    if params[:id].present?
      @user = Group.where(id: params[:id])
    end
    if request.xhr?
      respond_to do |format|
        format.json {
          render json: @user
        }
      end
    end
  end

  def beneficiary_type
    if params[:type].present?
      @teams = Group.where(type: params[:type])
    end
    if request.xhr?
      respond_to do |format|
        format.json {
          render json: @teams
        }
      end
    end
  end

  def beneficiary_name
    if params[:id].present?
      @user = Group.where(id: params[:id])
    end
    if request.xhr?
      respond_to do |format|
        format.json {
          render json: @user
        }
      end
    end
  end

  def submit
    if request.xhr?

      # find by id
      sender = Group.find_by_id(params[:sender])
      beneficiary = Group.find_by_id(params[:beneficiary])

      # doing the calculation
      sender_balance = sender['balance'].to_f - params[:amount].to_f
      beneficiary_balance = beneficiary['balance'].to_f + params[:amount].to_f

      if sender['balance'].to_f > params[:amount].to_f and sender['id'] != beneficiary['id']
        # update the balance
        sender.update_attributes!(:balance => sender_balance)
        beneficiary.update_attributes!(:balance => beneficiary_balance)

        respond_to do |format|
          format.json {
            render json: {status: 1, sender: sender_balance, beneficiary: beneficiary_balance}
          }
        end
      else
        respond_to do |format|
          format.json {
            render json: {status: 0, message: 'Insufficient Balance. Or Beneficiary name are the same as Sender.'}
          }
        end
      end

    end
  end

end
