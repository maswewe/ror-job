class Wallet < ActiveRecord::Base 
  has_many :groups
  delegate :user, :team, :stock, to: :groups
end
