class Group < ActiveRecord::Base 
    belongs_to :wallet
    self.inheritance_column = :type

    scope :user, -> { where(type: 'User') }
	  scope :team, -> { where(type: 'Team') }
	  scope :stock, -> { where(type: 'Stock') }

  class << self
    def types
      %w(User Team Stock)
    end
  end

end

