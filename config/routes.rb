Rails.application.routes.draw do
  get 'balance/index'
  get 'transfer/index'
  get 'transfer/sender_type'
  get 'transfer/sender_name'
  get 'transfer/beneficiary_type'
  get 'transfer/beneficiary_name'
  post 'transfer/submit'
  get 'home/index'
  
  resources :groups
  resources :users, controller: 'groups', type: 'User'
  resources :teams, controller: 'groups', type: 'Team'
  resources :stocks, controller: 'groups', type: 'Stock'

  root 'home#index'
end
